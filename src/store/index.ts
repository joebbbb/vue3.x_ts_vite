import {createStore} from 'vuex'

export default createStore({
    state: {
        count: 5,
        routes: []
    },
    mutations: {
        getCountType(state, data) {
            state.count = data
        },
        getRoutes(state, data) {
            state.routes = data
        },
    },
    actions: {
        setCountType({commit}, data) {
            commit('getCountType', data)
        },
        setRoutes({commit}, data) {
            commit('getRoutes', data)
        }
    }
})