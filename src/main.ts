import { createApp } from 'vue'
import router from './router'
import App from './App.vue'
import store from './store'
import axios from './utils/http'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import './theme/index.scss';
import 'dayjs/locale/zh-cn'
import locale from 'element-plus/lib/locale/lang/zh-cn'
const app  = createApp(App)
app.use(router).use(store).use(ElementPlus,{ locale }).mount('#app')
app.config.globalProperties.$axios = axios