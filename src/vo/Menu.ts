// 简单demo示例
export default class Menu {
    private _id: number;
    private _label: string;
    private _url: string | null;
    private _children: Menu[] | null;

    constructor(id: number, label: string, url: string | null, children: Menu[] | null) {
        this._id = id;
        this._label = label;
        this._url = url;
        this._children = children;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value;
    }

    get url(): string | null {
        return this._url;
    }

    set url(value: string | null) {
        this._url = value;
    }

    get children(): Menu[] | null {
        return this._children;
    }

    set children(value: Menu[] | null) {
        this._children = value;
    }
}