import axios from 'axios';
import { getSession } from './storage';
import qs from 'qs';
const service = axios.create({
	// baseURL: ApiUrl.url,
	baseURL:  import.meta.env.VITE_API_URL as any,
	timeout: 50000,
	headers: { 
		'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
	},
});

service.interceptors.request.use(
	(config) => {
		config.data = qs.stringify(config.data)
		if (getSession('token')) {
			config.headers.common['token'] = `${getSession('token')}`;
		}
		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

service.interceptors.response.use(
	(response) => {
		return response
	},
	(error) => {
		if (error.message.indexOf('timeout') != -1) {
            console.log('���糬ʱ')
		} else if (error.message == 'Network Error') {
            console.log('�������Ӵ���');
		}
		return Promise.reject(error);
	}
);

export default service;