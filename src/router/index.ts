import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router';
import {menuFn} from '../allApi/menu';
import {getSession, setSession} from '../utils/storage'
import store from '../store';

const dynamicViewsModules = import.meta.glob('../view/**/*.{vue,tsx}');
const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        component: () => import('../view/login/index.vue'),
    },
    {
        path: '/login',
        name: '登录',
        component: () => import('../view/login/index.vue')
    }

]

export function menuArray(params: Array<any>): Array<any> {
    params.forEach(item => {
        if (item.component) {
            Object.keys(dynamicViewsModules).filter(key => {
                if (item.component == key.replace('../view', '')) {
                    item.component = dynamicViewsModules[key]
                }
            })
        }
        if (item.children) menuArray(item.children)
    })
    console.log("什么垃圾?")
    return params
}

await menuFn().then((res: any) => {
    menuArray(res.data.data).forEach((route: any) => {
        routes.push({
            path: route.path,
            name: route.name,
            component: route.component,
            children: route.children
        })
        setSession('routes', res.data.data)
        store.dispatch('setRoutes', res.data.data)
    })
})
const router = createRouter({
    history: createWebHashHistory(),
    routes,
})


router.beforeEach(async (to, from, next) => {
    next()
});
export default router;